package cl.ubiobio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class ToDo {
	@Id
	@GeneratedValue
	
	private long id;
	private String titulo;
	private String descripcion;
	private boolean estado;
	private String categoria; 
	
	public ToDo(){
		
	}
	
	public long getId(){
		return id;
	}
	
	public void setId (long id){
		this.id=id;
	}
	
	public String getTitulo(){
		return titulo;
	}
	
	public void setTitulo(String titulo){
		this.titulo=titulo;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	
	public void SetDescripcion(String descripcion){
		this.descripcion=descripcion;
	}
	
	public boolean getEstado(){
		return estado;
	}
	
	public void SetEstado(boolean estado){
		this.estado=estado;
	}
	
	public String getCategoria(){
		return categoria;
	}
	
	public void setCategoria(String categoria){
		this.categoria= categoria;
	}
	
	

}
