package cl.ubiobio.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubiobio.model.ToDo;

public interface ToDoDao extends CrudRepository<ToDo, Long> {
	public ToDo findByCategoria(String categoria);
	public ToDo findByEstado(Boolean estado);

}

