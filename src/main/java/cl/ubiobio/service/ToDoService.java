package cl.ubiobio.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubiobio.dao.ToDoDao;
import cl.ubiobio.model.ToDo;


public class ToDoService {

	@Autowired
	private  ToDoDao tododao;
	
	public List<ToDo> crearToDoList(ToDo todo){
		List<ToDo> ToDoList = new ArrayList<ToDo>();
		return ToDoList;
	}
	
	public void addToDo(List<ToDo> ToDoList, ToDo todo) {
		ToDoList.add(todo);
	}

	public void deleteTodo(List<ToDo> ToDoList, ToDo todo) {
		ToDoList.remove(todo);
	}
	
	public List<ToDo> obtenerTodos() {
		// TODO Auto-generated method stub
		List<ToDo> ToDoList = new ArrayList<ToDo>();
		ToDoList = (ArrayList<ToDo>) tododao.findAll();
		return ToDoList;
	}

}
