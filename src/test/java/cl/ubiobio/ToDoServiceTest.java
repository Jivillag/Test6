package cl.ubiobio;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubiobio.dao.ToDoDao;
import cl.ubiobio.service.ToDoService;
import cl.ubiobio.model.ToDo;

@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {
	
	@Mock
	private ToDoDao tododao;

	@InjectMocks
	private ToDoService todoservice;
	
	@Test
	public void ObtenerTodosLosToDoLuis() {  
		
			//arrange
			List<ToDo> ToDoList = new ArrayList<ToDo>();
			List<ToDo> ToDoListResult = new ArrayList<ToDo>();
			
			//act
			when(tododao.findAll()).thenReturn(ToDoList);
			ToDoListResult = todoservice.obtenerTodos();
			
			//assert 
			Assert.assertNotNull(ToDoListResult);
			Assert.assertEquals(ToDoListResult, ToDoList);
		}
}
